const Discord = require('discord.js')
require('dotenv').config()
const client = new Discord.Client();
const { TeamSpeak, QueryProtocol } = require("ts3-nodejs-library")


//Globals
var poll = false
const prefix = '!'

// channels
const teamspeak_status = '749743248318136413'
const general_chat = '672156667487453184'

// Teamspeak code

const teamspeak = new TeamSpeak({
    host: process.env.TS_SERVER,
    protocol: QueryProtocol.RAW,
    queryport: process.env.TS_QUERY_PORT,
    serverport: process.env.TS_SERVER_PORT,
    nickname: "12eDiscordBot"
})



teamspeak.on("ready",()=>{
    ( async()=> { 
        const whoami = await teamspeak.whoami() 
        console.log(whoami)
        console.log("I am connected to the Teamspeak")
        console.log(teamspeak.channels)
        let list = await teamspeak.channelList()
        list.forEach(item => {
            console.log(item.propcache.channelName)
            console.log(item.propcache.cid)
        })
    } )()
})

teamspeak.on("textmessage", ev =>{
    console.log(ev.msg)
    if(client instanceof Discord.Client){
        if(ev.msg.startsWith(prefix_notify)){
            //let args = msg.content.slice(prefix_notify.length).trim().split(' ') //grab the argument
            let args = ev.msg.slice(prefix_notify.length).trim().split(' ')
            let arg = " "
            args.forEach(elem =>{
                arg = arg + elem + " "
            })
            client.channels.cache.get(general_chat).send(`There are 12e members playing ${arg}. Come join on Teamspeak! `)
        }
    }
})

teamspeak.on('error',(err)=>{
    console.log("Something went wrong")
    console.error(err)
    poll = false //stop polling the teamspeak
})

function sleep (ms){
    return new Promise(resolve =>{
        setTimeout(resolve,ms)
    })
}

//Discord code
const prefix_notify = '!12eNotify'


client.on('ready',()=>{
    console.log("Discord bot connected")
    if (teamspeak instanceof TeamSpeak){
        (async ()=>{
            console.log("Teamspeak connected")
            let clientsList = "\n"
            client.channels.cache.get(teamspeak_status).send('Bot connected to Teamspeak')
            poll = true // begin polling
            while(poll){
                const serverInfo = await teamspeak.serverInfo()
                const clients = await teamspeak.clientList()
                clients.forEach(client=>{
                    console.log(client.propcache.clientNickname)
                    clientsList = clientsList + client.propcache.clientNickname + " \n"
                })
                console.log(clients.length)
                client.channels.cache.get(teamspeak_status).send(`**${serverInfo.virtualserverName}** Status: **${serverInfo.virtualserverStatus}**\nThere are **${serverInfo.virtualserverClientsonline}** users online.\nThey are: \`\`\`${clientsList}\`\`\` `)
                await sleep(7200000) //1 hour
                clientsList = "\n"
            }
       })()
        
    }
    else{
            console.error("Teamspeak did not connect")
    }

})

client.on('message',(msg)=>{
    console.log(`saw message: ${msg}`)
    if(msg.content.startsWith(prefix_notify)){
        let args = msg.content.slice(prefix_notify.length).trim().split(' ') //grab the argument
        let arg = ""
        console.log(args)
        args.forEach(elem =>{
            arg = arg + elem + " "
        })
        console.log(arg)
        if(arg.length>0){
            client.channels.cache.get(general_chat).send(`There are 12e members playing ${arg}. Come join on Teamspeak! `)
        }
    }
})

client.login(process.env.TOKEN)

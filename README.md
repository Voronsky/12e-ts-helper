# 12e-ts-helper

Teamspeak3 Helper  bot for 12e Teamspeak

# How to install

Be sure to install all the dependencies first via: 

```
npm install
```

# Environment file

For obvious reasons, practical examples of tokens and credentials have been ommitted. The code uses heavily '.env' files. 
Make sure to have the proper variables set